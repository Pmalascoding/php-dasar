<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Looping e JCC</title>
</head>
<body>
    <?php 

    echo "<h3>Soal 1</h3>";
    echo "<h5> Looping 1 </h5>";
    for ($i=0; $i <= 20; $i+=2) {
        echo $i . " - I Love  PHP <br>";
    }

    echo "<h5>Looping 2</h5>";

    $j = 20;
    do {
        echo "$j - I Love PHP  <br>";
        $j-=2;

    } while ($j >= 1);

    echo "<h3> Soal 2 </h3>";

    $nomor = [18, 45, 29, 61, 47, 34];
    echo "Array numbers : ";
    print_r($nomor);
    foreach ($nomor as $value) {
        $items[] =  $value%5;
    }
    echo "<br>";
    echo "Sisa baginya adalah : ";
    print_r($items);

    echo "<h3>soal 3</h3>";

    $biodata = [
        ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
        ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
        ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
        ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
    ];
    foreach ($biodata as $key => $value) {
        $array = [
            'id' => $value[0],
            'name' => $value[1],
            'price' => $value[2],
            'description' => $value[3],
            'source' => $value[4],
        ];
        print_r($array);
        echo "<br>;";
    }
    echo "<h3>Soal 4</h3>";
    $o = 5;
    for ($i=$o; $i > 0; $i--) { 
        for ($j=$o; $j >= $i ; $j--) { 
            echo "*";
        }
        echo "<br>";
    }

    
    
    ?>
</body>
</html>