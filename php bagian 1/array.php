<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>array php dasar</title>
</head>
<body>
    <h1>Array</h1>
    <?php 
        echo "<h3>Soal 1</h3>";
        $kids =["Mike", "Dustin", "will", "Lucas","Max"];
        $adults =["Hopper", "Nancy", "Joyce", "Jonathan","Murray"];

        print_r($kids);
        echo "<br>";
        print_r($adults);

        echo "<h3>Soal 2</h3>";
        echo "total : " . count($kids)."<br>";
        echo "total : " . count($adults)."<br>";
        echo "<ol>";
        echo "<li>".$kids[0]."</li>";
        echo "<li>".$kids[1]."</li>";
        echo "<li>".$kids[2]."</li>";
        echo "<li>".$kids[3]."</li>";
        echo "<li>".$kids[4]."</li>";
        echo "</ol>";
        echo "<ol>";
        echo "<li>".$adults[0]."</li>";
        echo "<li>".$adults[1]."</li>";
        echo "<li>".$adults[2]."</li>";
        echo "<li>".$adults[3]."</li>";
        echo "<li>".$adults[4]."</li>";
        echo "</ol>";

        echo "SOAL 3";
        $biodata = [
            ["nama"=> "will Byers","age"=>12, "aliasses"=>"will the wise","status" => "Alive"],
            ["nama"=> "will Byers","age"=>12, "aliasses"=>"Dungaon Master","status" => "Alive"],
            ["nama"=> "will Byers","age"=>43, "aliasses"=>"Chief Hopper","status" => "Alive"],
            ["nama"=> "will Byers","age"=>12, "aliasses"=>"el  ","status" => "Alive"]

        ];
        echo "<pre>";
        print_r($biodata);
        echo "</pre>";
    ?>
</body>
</html>