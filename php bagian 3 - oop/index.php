<?php 

require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new animal("shaun");
echo "Nama = " . $sheep -> type;
echo "<br>";
echo "legs = " . $sheep -> legs;
echo "<br>";
echo "cold_bloded = " . $sheep -> cold_bloded;
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Nama = " . $kodok -> type;
echo "<br>";
echo "legs = " . $kodok -> legs;
echo "<br>";
echo "cold_bloded = " . $kodok -> cold_bloded;
echo "<br>";
echo "jump = " . $kodok -> jump();
echo "<br><br>";


$sungokong = new Ape("Kera sakti");
echo "Nama = " . $sungokong -> type;
echo "<br>";
echo "legs = " . $sungokong -> legs;
echo "<br>";
echo "cold_bloded = " . $sungokong -> cold_bloded;
echo "<br>";
echo "yell = " . $sungokong -> yell();
echo "<br><br>";


?>